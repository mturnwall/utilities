import {getRandom} from './getRandom';

const RADIX = 16;

const simulateUUID = () => {
    const time_low = getRandom(8, RADIX);
    const time_mid = getRandom(4, RADIX);
    const time_hi_and_version = `4${getRandom(3, RADIX)}`;
    const clock_seq_hi = getRandom(4, RADIX);
    const node = getRandom(12, RADIX);
    return `${time_low}-${time_mid}-${time_hi_and_version}-${clock_seq_hi}-${node}`;
};

export {
    simulateUUID,
};
