// Requires https://www.npmjs.com/package/@fdaciuk/ajax
import ajax from '@fdaciuk/ajax';

/**
 *  Object that holds all the API endpoints.
 *  @example
 *  {
 *      apiName: '/apiPath'
 *  }
 */
// const endpoints = {};

/**
 * @param {string} apiHost - The URL (host) of your API
 * @param {string} apiPath - The path to the API, this is the part that comes after the host
 * @param {object} endpoints - Object that holds all the API endpoints.
 * @example
 *  {
 *      apiName: '/apiPath'
 *  }
 * @return {api~inner} - returned function
 */
const api = function (apiHost, apiPath, endpoints = {}) {
    /**
     * @param {string} endpoint - api endpoint to call
     * @param {*} data - data that will be sent to the server
     * @param {string} [method=POST] - the http action such as GET or POST
     * @returns {Promise} - Promise wrapper for the ajax call
     */
    return function ({endpoint, data, method = 'POST'} = {}) {
        return new Promise((resolve, reject) => {
            if (!(endpoint in endpoints)) {
                reject(new Error(`The api endpoint "${endpoint}" was not found`));
                return false;
            }
            ajax({
                method,
                data,
                url: `${apiHost}${apiPath}${endpoints[endpoint]}`
            })
                .then(response => {
                    resolve(response);
                })
                .catch(err => {
                    reject(err);
                });
            return true;
        });
    };
};

export {
    api,
};
