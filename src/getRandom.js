const DEFAULT_RADIX = 10;
const LENGTH_PADDING = 2;

const getRandom = (length = 2, radix = DEFAULT_RADIX) => {
    const beginIndex = length > 0 ? LENGTH_PADDING : length;
    if (length < 0) {
        return Math.random()
            .toString(radix)
            .slice(beginIndex);
    }
    return Math.random()
        .toString(radix)
        .slice(beginIndex, length + LENGTH_PADDING);
};

const getRandomBool = () => Math.random() >= 0.5;

export {getRandom, getRandomBool};
