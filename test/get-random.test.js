/* eslint-disable no-undef, no-unused-expressions */

import 'chai/register-expect';
import {getRandom, getRandomBool} from '../index';

describe('getRandom', function () {
    it('Random number returned equals the length passed', function () {
        const ranNum = getRandom(10);
        expect(ranNum).to.have.lengthOf(10);
    });
    it('Random number should be alphanumeric', function () {
        const ranNum = getRandom(12, 16);
        expect(/[a-f]/.test(ranNum)).to.be.true;
    });
    it('A boolean is returned from getRandomBool()', function () {
        const randBool = getRandomBool();
        expect(randBool).to.be.a('boolean');
    });
});
