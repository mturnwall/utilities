# Utilities

[![codebeat badge](https://codebeat.co/badges/565d7f4c-ca2e-484f-b1ee-d18b8d623877)](https://codebeat.co/projects/gitlab-com-mturnwall-utilities-master)

Just some utilities that I'm using at work.

## Usage

### getRandom(length, [radix])

This function generates a random number to the length you provide. It takes an option second parameter to set the radix (base) of the number.

```javascript
getRandom(8);    // 05567070
getRandom(8, 16) // c49dee9f
```

### getRandomBool()

Just a simple function that will return a random boolean (true/false).

### simulateUUID()

This will return a random number in the format of a UUID version 4. It's not a true UUID though since it just uses random numbers.

```javascript
simulateUUID(); // f62759bd-b37a-4971-be93-821313573856
```

### api(apiHost, apiPath, endpoints)({endpoint, data, method})

This function can be used to make API calls. It's a curry function so when you first call it you need to assign it to a variable. `api()` acts a wrapper for the `ajax` module by @fdaciuk. It wraps the module inside a `Promise`.

#### Parameters

apiHost - {string} Host (URL) of the api
apiPath - {string} The path to the api
endpoints - {object} This is an object that holds the api resources.

#### Example

```javascript
// setup the api
// So the full URL to the "locations" resource would be:
// https://private-0fde15-pto.apiary-mock.com/pto/locations
const myApi = api("https://private-0fde15-pto.apiary-mock.com", "/pto", {
  locations: "/locations"
});
// make api call
myApi({
    endpoint: 'locations',
    method: 'GET'
}).then(response => {
    console.log(response);
}).catch(err => {
    console.warn('API:', err.message);
});
```

## License

[MIT](https://choosealicense.com/licenses/mit/)
